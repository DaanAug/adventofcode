<?php

$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$result = 0;

while (($row = fgetcsv($handle, separator: "\n")) !== false) {
    $inputString = $row[0];

    preg_match_all('(mul\(\d{1,3},\d{1,3}\))', $inputString, $matches);

    foreach ($matches[0] as $match) {
        $result += calculate($match);
    }
}

function calculate($string): int
{
    $numbers = explode(',', str_replace(['mul(', ')'], '', $string));

    return $numbers[0] * $numbers[1];
}

echo "<h2>Answer: $result</h2>";
