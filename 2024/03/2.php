<?php

$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$result = 0;

$active = true;

while (($row = fgetcsv($handle, separator: "\n")) !== false) {
    $inputString = $row[0];

    preg_match_all("((mul\(\d{1,3},\d{1,3}\))|(don't\(\))|(do\(\)))", $inputString, $matches);

    foreach ($matches[0] as $match) {
        if ($match === "don't()") {
            $active = false;
            continue;
        }

        if ($match === "do()") {
            $active = true;
            continue;
        }

        if ($active) {
            $result += calculate($match);
        }
    }
}

function calculate($string): int
{
    $numbers = explode(',', str_replace(['mul(', ')'], '', $string));

    return $numbers[0] * $numbers[1];
}

echo "<h2>Answer: $result</h2>";
