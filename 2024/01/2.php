<?php

$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$firstItems = [];
$lastItems = [];
$itemsCount = 0;

while (($row = fgetcsv($handle, separator: ' ')) !== false) {
    $firstItems[] = intval($row[0]);
    $lastItems[] = intval(end($row));
    $itemsCount++;
};

sort($firstItems);
sort($lastItems);

$answer = 0;

foreach ($firstItems as $firstItem) {
    $amountSeen = 0;
    foreach ($lastItems as $lastItem) {
        if ($lastItem > $firstItem) {
            break;
        }

        if ($firstItem === $lastItem) {
             $amountSeen++;
        }
    }

    $answer += $amountSeen * $firstItem;
}

echo "<h2>Answer: $answer</h2>";
