<?php

$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$firstItems = [];
$lastItems = [];
$itemsCount = 0;

while (($row = fgetcsv($handle, separator: ' ')) !== false) {
    $firstItems[] = $row[0];
    $lastItems[] = end($row);
    $itemsCount++;
};

sort($firstItems);
sort($lastItems);

$totalDistance = 0;

for ($i = 0; $i < $itemsCount; $i++) {
    $firstItem = intval($firstItems[$i]);
    $lastItem = intval($lastItems[$i]);

    $totalDistance += abs($firstItem - $lastItem);
}

echo "<h2>Answer: $totalDistance</h2>";
