<?php

$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$rules = [];
$totalPages = [];
$result = 0;

while (($row = fgetcsv($handle, separator: "\n")) !== false) {
    if (!isset($row[0])) {
        continue;
    }

    $rowString = (trim($row[0]));

    if (str_contains($rowString, '|')) {
        $rules[] = explode('|', $rowString);
    }

    if (str_contains($rowString, ',')) {
        $totalPages[] = explode(',', $rowString);
    }
}

foreach ($totalPages as $pages) {
    // Check if the pages are in the correct order according to the rules
    $pages = array_map('intval', $pages);

    if (isCorrectOrder($pages, $rules)) {
        continue;
    }

    // I couldn't find a linear solution :(
    while (!isCorrectOrder($pages, $rules)) {
        $pages = putInCorrectOrder($pages, $rules);
    }

    // Get the middle page
    $middlePageIndex = (count($pages) - 1) / 2;
    $result += $pages[$middlePageIndex];
}

echo "<h2>Answer: $result</h2>";

function isCorrectOrder($pages, $rules) {
    foreach ($rules as $rule) {
        $firstPage = $rule[0];
        $secondPage = $rule[1];

        if (in_array($firstPage, $pages) && in_array($secondPage, $pages)) {
            $firstPageIndex = array_search($firstPage, $pages);
            $secondPageIndex = array_search($secondPage, $pages);

            if ($firstPageIndex > $secondPageIndex) {
                return false;
            }
        }
    }

    return true;
}

function putInCorrectOrder($pages, $rules) {
    foreach ($rules as $rule) {
        $firstPage = $rule[0];
        $secondPage = $rule[1];

        if (in_array($firstPage, $pages) && in_array($secondPage, $pages)) {
            $firstPageIndex = array_search($firstPage, $pages);
            $secondPageIndex = array_search($secondPage, $pages);

            if ($firstPageIndex > $secondPageIndex) {
                // Swap the pages
                $temp = $pages[$firstPageIndex];
                $pages[$firstPageIndex] = $pages[$secondPageIndex];
                $pages[$secondPageIndex] = $temp;
            }
        }
    }

    return $pages;
}
