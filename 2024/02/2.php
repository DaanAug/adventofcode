<?php
$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$result = 0;

while (($row = fgetcsv($handle, separator: ' ')) !== false) {
    if (isValid($row)) {
        $result++;

        continue;
    }

    // Check if it's a valid row when removing an item
    for ($i = 0; $i < count($row); $i++) {
        $newRow = $row;

        array_splice($newRow, $i, 1);

        if (isValid($newRow)) {
            $result++;

            break;
        }
    }
}

function isValid($row): bool
{
    $ascending = true;

    if (count($row) < 2) {
        return false;
    }

    if (intval($row[0]) > intval($row[1])) {
        $ascending = false;
    }

    for ($i = 0; $i < count($row) - 1; $i++) {
        $item1 = intval($row[$i]);
        $item2 = intval($row[$i + 1]);

        $diff = abs($item1 - $item2);

        if ($diff < 1 || $diff > 3) {
            return false;
        }

        if ($ascending && $item1 > $item2) {
            return false;
        }

        if (!$ascending && $item1 < $item2) {
            return false;
        }
    }

    return true;
}

echo "<h2>Answer: $result</h2>";
