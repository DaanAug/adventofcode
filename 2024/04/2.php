<?php
const INPUT_DATA_SIZE = 140;

$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$result = 0;
$inputData = [];

while (($row = fgetcsv($handle, separator: "\n")) !== false) {
    $rowData = str_split($row[0], 1);
    $inputData[] = $rowData;
}

for ($i = 0; $i < INPUT_DATA_SIZE; $i++) {
    for ($j = 0; $j < INPUT_DATA_SIZE; $j++) {
        if ($i + 1 >= INPUT_DATA_SIZE
            || $j - 1 < 0
            || $i - 1 < 0
            || $j + 1 >= INPUT_DATA_SIZE) {
            continue;
        }

        $currentChar = $inputData[$i][$j];

        if ($currentChar !== 'A') {
            continue;
        }

        $searchDiagonalLeftDownToRightUp = searchDiagonalLeftDownToRightUp($i, $j, $inputData);
        $searchDiagonalLeftUpToRightDown = searchDiagonalLeftUpToRightDown($i, $j, $inputData);

        if ($searchDiagonalLeftDownToRightUp && $searchDiagonalLeftUpToRightDown) {
            $result++;
        }
    }
}

function searchDiagonalLeftDownToRightUp($i, $j, $inputData)
{
    $letterLeftDown = $inputData[$i + 1][$j - 1];
    $letterRightUp = $inputData[$i - 1][$j + 1];

    if ($letterLeftDown === 'M' && $letterRightUp === 'S') {
        return true;
    }

    if ($letterLeftDown === 'S' && $letterRightUp === 'M') {
        return true;
    }

    return false;
}

function searchDiagonalLeftUpToRightDown($i, $j, $inputData)
{
    $letterLeftUp = $inputData[$i - 1][$j - 1];
    $letterRightDown = $inputData[$i + 1][$j + 1];

    if ($letterLeftUp === 'M' && $letterRightDown === 'S') {
        return true;
    }

    if ($letterLeftUp === 'S' && $letterRightDown === 'M') {
        return true;
    }

    return false;
}

echo "<h2>Answer: $result</h2>";
