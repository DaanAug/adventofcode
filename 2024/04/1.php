<?php
const SEARCH_WORD = 'XMAS';
const INPUT_DATA_SIZE = 140;

$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$result = 0;
$inputData = [];

while (($row = fgetcsv($handle, separator: "\n")) !== false) {
    $rowData = str_split($row[0],1);
    $inputData[] = $rowData;
}

for ($i = 0; $i < INPUT_DATA_SIZE; $i++) {
    for ($j = 0; $j < INPUT_DATA_SIZE; $j++) {
        $currentChar = $inputData[$i][$j];

        if ($currentChar !== 'X'){
            continue;
        }

        // Search in all directions for the word XMAS

        if (searchRight($i, $j, $inputData)) {
            $result++;
        }

        if (searchLeft($i, $j, $inputData)) {
            $result++;
        }

        if (searchDown($i, $j, $inputData)) {
            $result++;
        }

        if (searchUp($i, $j, $inputData)) {
            $result++;
        }

        if (searchDiagonalRightDown($i, $j, $inputData)) {
            $result++;
        }

        if (searchDiagonalRightUp($i, $j, $inputData)) {
            $result++;
        }

        if (searchDiagonalLeftDown($i, $j, $inputData)) {
            $result++;
        }

        if (searchDiagonalLeftUp($i, $j, $inputData)) {
            $result++;
        }
    }
}

function searchRight($i, $j, $inputData) {
    $searchWordLength = strlen(SEARCH_WORD);

    for ($k = 0; $k < $searchWordLength; $k++) {
        if ($j + $k >= INPUT_DATA_SIZE) {
            return false;
        }

        if ($inputData[$i][$j + $k] !== SEARCH_WORD[$k]) {
            return false;
        }
    }

    return true;
}

function searchLeft($i, $j, $inputData) {
    $searchWordLength = strlen(SEARCH_WORD);

    for ($k = 0; $k < $searchWordLength; $k++) {
        if ($j - $k < 0) {
            return false;
        }

        if ($inputData[$i][$j - $k] !== SEARCH_WORD[$k]) {
            return false;
        }
    }

    return true;
}

function searchDown($i, $j, $inputData) {
    $searchWordLength = strlen(SEARCH_WORD);

    for ($k = 0; $k < $searchWordLength; $k++) {
        if ($i + $k >= INPUT_DATA_SIZE) {
            return false;
        }

        if ($inputData[$i + $k][$j] !== SEARCH_WORD[$k]) {
            return false;
        }
    }

    return true;
}

function searchUp($i, $j, $inputData) {
    $searchWordLength = strlen(SEARCH_WORD);

    for ($k = 0; $k < $searchWordLength; $k++) {
        if ($i - $k < 0) {
            return false;
        }

        if ($inputData[$i - $k][$j] !== SEARCH_WORD[$k]) {
            return false;
        }
    }

    return true;
}

function searchDiagonalRightDown($i, $j, $inputData) {
    $searchWordLength = strlen(SEARCH_WORD);

    for ($k = 0; $k < $searchWordLength; $k++) {
        if ($i + $k >= INPUT_DATA_SIZE || $j + $k >= INPUT_DATA_SIZE) {
            return false;
        }

        if ($inputData[$i + $k][$j + $k] !== SEARCH_WORD[$k]) {
            return false;
        }
    }

    return true;
}

function searchDiagonalRightUp($i, $j, $inputData) {
    $searchWordLength = strlen(SEARCH_WORD);

    for ($k = 0; $k < $searchWordLength; $k++) {
        if ($i - $k < 0 || $j + $k >= INPUT_DATA_SIZE) {
            return false;
        }

        if ($inputData[$i - $k][$j + $k] !== SEARCH_WORD[$k]) {
            return false;
        }
    }

    return true;
}

function searchDiagonalLeftDown($i, $j, $inputData) {
    $searchWordLength = strlen(SEARCH_WORD);

    for ($k = 0; $k < $searchWordLength; $k++) {
        if ($i + $k >= INPUT_DATA_SIZE || $j - $k < 0) {
            return false;
        }

        if ($inputData[$i + $k][$j - $k] !== SEARCH_WORD[$k]) {
            return false;
        }
    }

    return true;
}

function searchDiagonalLeftUp($i, $j, $inputData) {
    $searchWordLength = strlen(SEARCH_WORD);

    for ($k = 0; $k < $searchWordLength; $k++) {
        if ($i - $k < 0 || $j - $k < 0) {
            return false;
        }

        if ($inputData[$i - $k][$j - $k] !== SEARCH_WORD[$k]) {
            return false;
        }
    }

    return true;
}



echo "<h2>Answer: $result</h2>";
