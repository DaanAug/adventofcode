<?php

$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$obstacleSymbol = '#';
$emptySymbol = '.';
$guardSymbol = '^';

$map = [];

while (($row = fgetcsv($handle, separator: "\n")) !== false) {
    if (!isset($row[0])) {
        continue;
    }

    $rowString = (trim($row[0]));

    $map[] = str_split($rowString);
}

$mapWidth = count($map[0]);
$mapHeight = count($map);

$emptyMap = array_fill(0, $mapHeight, array_fill(0, $mapWidth, 0));

$guardPosition = [0,0];

for ($i = 0; $i < count($map); $i++) {
    for ($j = 0; $j < count($map[$i]); $j++) {
        if ($map[$i][$j] === $guardSymbol) {
            $guardPosition = [$i, $j];
        }
    }
}

// Rules: Guard starts moving up, when it hits a wall it turns right.
// When it hit the border it is free.
// Mark the path of the guard with 1s

$directions = [
    [-1, 0], // up
    [0, 1], // right
    [1, 0], // down
    [0, -1], // left
];

$currentDirection = 0;

$currentPosition = $guardPosition;

while (true) {
    $emptyMap[$currentPosition[0]][$currentPosition[1]] = 1;

    $nextPosition = [
        $currentPosition[0] + $directions[$currentDirection][0],
        $currentPosition[1] + $directions[$currentDirection][1],
    ];

    if ($nextPosition[0] < 0 || $nextPosition[0] >= $mapHeight || $nextPosition[1] < 0 || $nextPosition[1] >= $mapWidth) {
        // End of the map
        break;
    }

    if ($map[$nextPosition[0]][$nextPosition[1]] === $obstacleSymbol) {
        // Change the direction
        $currentDirection = ($currentDirection + 1) % 4;

        continue;
    }

    // Update position
    $currentPosition = $nextPosition;
}

$markedPositionsCount = 0;

foreach ($emptyMap as $row) {
    $markedPositionsCount += array_sum($row);
}

echo "<h2>Answer: $markedPositionsCount</h2>";
