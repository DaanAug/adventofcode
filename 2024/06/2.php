<?php

$inputFile = './input.csv';
$handle = fopen('./input.csv', 'r');

$obstacleSymbol = '#';
$emptySymbol = '.';
$guardSymbol = '^';

$map = [];

while (($row = fgetcsv($handle, separator: "\n")) !== false) {
    if (!isset($row[0])) {
        continue;
    }

    $rowString = (trim($row[0]));

    $map[] = str_split($rowString);
}

$mapWidth = count($map[0]);
$mapHeight = count($map);

$emptyMap = array_fill(0, $mapHeight, array_fill(0, $mapWidth, 0));

$guardPosition = guardStartingPosition($map);

// Rules: Guard starts moving up, when it hits a wall it turns right.
// When it hit the border it is free.
// Mark the path of the guard with 1s

$directions = [
    [-1, 0], // up
    [0, 1], // right
    [1, 0], // down
    [0, -1], // left
];

$currentDirection = 0;

$currentPosition = $guardPosition;

while (true) {
    $emptyMap[$currentPosition[0]][$currentPosition[1]] = 1;

    $nextPosition = [
        $currentPosition[0] + $directions[$currentDirection][0],
        $currentPosition[1] + $directions[$currentDirection][1],
    ];

    if ($nextPosition[0] < 0 || $nextPosition[0] >= $mapHeight || $nextPosition[1] < 0 || $nextPosition[1] >= $mapWidth) {
        // End of the map
        break;
    }

    if ($map[$nextPosition[0]][$nextPosition[1]] === $obstacleSymbol) {
        // Change the direction
        $currentDirection = ($currentDirection + 1) % 4;

        continue;
    }

    // Update position
    $currentPosition = $nextPosition;
}

// We now need to place an obstacle to make the guard walk in a loop
// Find all places to place an obstacle
$result = 0;
// We place an obstacle on every place the guard has walked and check if it is a loop
for ($i = 0; $i < $mapHeight; $i++) {
    for ($j = 0; $j < $mapWidth; $j++) {
        if ($emptyMap[$i][$j] === 1 && [$i, $j] !== $guardPosition) {
            $mapCopy = $map;
            $mapCopy[$i][$j] = $obstacleSymbol;

            $guardPosition = guardStartingPosition($mapCopy);

            $isLoop = walkGuard($mapCopy, $guardPosition, $directions);

            if ($isLoop) {
                $result++;
            }
        }
    }
}

echo "<h2>Answer: $result</h2>";

function guardStartingPosition($map): array
{
    for ($i = 0; $i < count($map); $i++) {
        for ($j = 0; $j < count($map[$i]); $j++) {
            if ($map[$i][$j] === '^') {
                return [$i, $j];
            }
        }
    }

    return [0, 0];
}

function walkGuard($map, $guardPosition, $directions)
{
    $startingPosition = $guardPosition;
    $startingDirection = 0;

    $mapWidth = count($map[0]);
    $mapHeight = count($map);

    $emptyMap = array_fill(0, $mapHeight, array_fill(0, $mapWidth, 0));

    $currentDirection = 0;

    $currentPosition = $guardPosition;

    $count = 0;

    while ($count < 10000) {
        $count++;

        $emptyMap[$currentPosition[0]][$currentPosition[1]] = 1;

        $nextPosition = [
            $currentPosition[0] + $directions[$currentDirection][0],
            $currentPosition[1] + $directions[$currentDirection][1],
        ];

        if ($nextPosition === $startingPosition && $currentDirection === $startingDirection) {
            // We have walked in a loop
            return true;
        }

        if ($nextPosition[0] < 0 || $nextPosition[0] >= $mapHeight || $nextPosition[1] < 0 || $nextPosition[1] >= $mapWidth) {
            // End of the map, it is not a loop
            return false;
        }

        if ($map[$nextPosition[0]][$nextPosition[1]] === '#') {
            // Change the direction
            $currentDirection = ($currentDirection + 1) % 4;

            continue;
        }

        // Update position
        $currentPosition = $nextPosition;
    }

    return true;
}
