<!DOCTYPE html>
<html>
<head>
    <title>Advent of code 2024</title>
</head>
<body>
    <h1>2024</h1>
    <ul>
        <?php
        // Function to recursively get PHP files and their directories
        function getPhpFilesAndDirectories($directory) {
            $result = [];
            $iterator = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($directory));

            foreach ($iterator as $file) {
                if ($file->isFile() && $file->getExtension() === 'php') {
                    $result[] = [
                        'file' => $file->getPathname(),
                        'directory' => $file->getPath()
                    ];
                }
            }

            return $result;
        }

        // Directory containing your PHP scripts
        $scriptDirectory = './';

        // Get PHP files and their directories recursively
        $phpFilesAndDirectories = getPhpFilesAndDirectories($scriptDirectory);

        usort($phpFilesAndDirectories, function ($a, $b) {
            return strcmp($a['file'], $b['file']);
        });

        $phpFilesAndDirectories = array_reverse($phpFilesAndDirectories);

        // Iterate through the files and display folder names and links
        foreach ($phpFilesAndDirectories as $fileInfo) {
            $filename = basename($fileInfo['file']);
            $directory = basename($fileInfo['directory']);
            if ($directory === '.') {
                continue;
            }
            echo '<li>' . '<a href="' . '2024/' . $fileInfo['file'] . '">' . $directory . ' - ' . $filename . '</a></li>';
        }
        ?>
    </ul>
</body>
</html>
