<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

while (($row = fgetcsv($handle, separator: ':')) !== false) {
    $id = substr($row[0], 5);

    $rounds = explode(';', $row[1]);

    $value = powerOfSet($rounds);

    $total += $value;
}

echo "<h2>Answer: $total</h2>";

fclose($handle);

function powerOfSet($rounds): int
{
    $values = [
        'red' => 0,
        'green' => 0,
        'blue' => 0,
    ];

    $sets = array_map(fn($round) => explode(',', $round), $rounds);

    $grabs = array_merge(...$sets);

    foreach ($grabs as $grab) {
        $grabArray = explode(' ', trim($grab));

        $values[$grabArray[1]] = max($values[$grabArray[1]], intval($grabArray[0]));
    }

    $power = 1;

    foreach ($values as $value) {
        $power *= $value;
    }

    return $power;
}

require_once('../helpers.php'); printFile(__FILE__);
