<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

while (($row = fgetcsv($handle, separator: ':')) !== false) {
    $id = substr($row[0], 5);

    $rounds = explode(';', $row[1]);

    $possible = true;

    foreach ($rounds as $round) {
        if (!roundIsPossible($round)) {
            $possible = false;

            break;
        }
    }

    if (!$possible) {
        continue;
    }

    $total += intval($id);
}

echo "<h2>Answer: $total</h2>";

fclose($handle);

function roundIsPossible($round): bool
{
    $maxValues = [
        'red' => 12,
        'green' => 13,
        'blue' => 14,
    ];

    $pulls = explode(',', $round);

    foreach ($pulls as $pull) {
        $pullArray = explode(' ', trim($pull));

        if ($pullArray[0] > $maxValues[$pullArray[1]]) {
            return false;
        }
    }

    return true;
}

require_once('../helpers.php'); printFile(__FILE__);
