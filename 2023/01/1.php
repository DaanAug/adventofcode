<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

while (($row = fgetcsv($handle)) !== false) {
    $line = trim($row[0]);

    $total+= calculateValue($line);
}

echo "<h2>Answer: $total</h2>";

fclose($handle);


function calculateValue($line)
{

    $characters = str_split($line);

    $firstNumericCharacter = firstNumericCharacter($characters);
    $lastNumericCharacter = firstNumericCharacter(array_reverse($characters));

    $value = $firstNumericCharacter . $lastNumericCharacter;

    return intval($value);
}

function firstNumericCharacter($characters)
{
    foreach ($characters as $character) {
        if (is_numeric($character)) {
            return $character;
        }
    }
}

require_once('../helpers.php'); printFile(__FILE__);
