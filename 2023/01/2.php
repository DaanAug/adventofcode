<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

while (($row = fgetcsv($handle)) !== false) {
    $line = trim($row[0]);

    $total+= calculateValue($line);
}

echo "<h2>Answer: $total</h2>";

fclose($handle);

function calculateValue($line): int
{
    $firstNumericCharacter = firstNumericCharacter($line);
    $lastNumericCharacter = firstNumericCharacter($line, true);

    $value = $firstNumericCharacter . $lastNumericCharacter;

    return intval($value);
}

function firstNumericCharacter($line, $reverse = false): int
{
    $digitsAsWords = [
        'zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'
    ];

    for ($i = 0; $i < strlen($line) + 1; $i++) {
        $candidates = [];

        $multiplier = $reverse ? -1 : 1;

        $offset = $reverse && $i === 0 ? 1 : $i;

        $substring = substr($line, $offset * $multiplier, min(strlen($line), 5));

        $characters = str_split($substring);

        if ($reverse) {
            $characters = array_reverse($characters);
        }

        foreach ($characters as $index => $character) {
            if (is_numeric($character)) {
                $candidates[$index] = $character;
            }
        }

        foreach ($digitsAsWords as $key => $word) {
            $pos = strpos($substring, $word);

            if ($pos !== false) {
                $candidates[$pos] = $key;
            }
        }

        if (!empty($candidates)) {
            ksort($candidates);

            return reset($candidates);
        }
    }

    return 0;
}

require_once('../helpers.php'); printFile(__FILE__);
