<?php

function printFile($file) {
    $contentArray = explode("\n", file_get_contents($file));

    $linesToPrint = array_slice($contentArray, 0, -2);

    $formattedLines = implode("\n", $linesToPrint);

    echo '<pre>' . htmlspecialchars($formattedLines) . '</pre>';
}