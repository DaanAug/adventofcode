<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

$symbols = ['!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '+', '=', '/'];

$lines = [];

while (($row = fgetcsv($handle)) !== false) {
    $lines[] = $row[0];
}

foreach ($lines as $index => $line) {
    $total += processLine($line, $index);
}

function processLine($line, $lineIndex)
{
    $value = 0;

    for($i = 0; $i < strlen($line); $i++) {
        $subLine = substr($line, $i, 3);

        if (!is_numeric(substr($subLine, 0, 1))) {
            continue;
        }

        $number = numberFromSubstr($subLine);

        $numberLength = strlen($number);

        if (isValidPartNumber($number, $lineIndex, $i)) {
            $value += $number;
        }

        $i += $numberLength - 1;
    }

    return $value;
}

function numberFromSubstr($line): int
{
    $characters = str_split($line);

    $numberCharacters = '';

    foreach ($characters as $character) {
        if (!is_numeric($character)) {
            break;
        }

        $numberCharacters .= $character;
    }

    return intval($numberCharacters);
}

function isValidPartNumber($number, $lineIndex, $columnIndex): bool
{
    global $lines;
    global $symbols;

    $numberLength = strlen($number);

    $currentLine = $lines[$lineIndex];

    $currentLineCharacters = str_split($currentLine);

    if (in_array($currentLineCharacters[$columnIndex - 1] ?? '', $symbols)) {
        return true;
    }

    if (in_array($currentLineCharacters[$columnIndex + $numberLength] ?? '', $symbols)) {
        return true;
    }

    $previousLine = $lines[$lineIndex - 1] ?? '';

    $previousLineSubstr = substr($previousLine,  max($columnIndex - 1, 0), $numberLength + 2);

    $previousLineCharacters = str_split($previousLineSubstr);

    foreach ($previousLineCharacters as $previousLineCharacter) {
        if (in_array($previousLineCharacter, $symbols)) {
            return true;
        }
    }

    $nextLine = $lines[$lineIndex + 1] ?? '';

    $nextLineSubstr = substr($nextLine, max($columnIndex - 1, 0), $numberLength + 2);

    $nextLineCharacters = str_split($nextLineSubstr);

    foreach ($nextLineCharacters as $nextLineCharacter) {
        if (in_array($nextLineCharacter, $symbols)) {
            return true;
        }
    }

    return false;
}

echo "<h2>Answer: $total</h2>";

fclose($handle);

require_once('../helpers.php'); printFile(__FILE__);
