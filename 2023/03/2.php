<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

$lines = [];

// Create placeholder array to mark gear positions in.
$marks = createRectangularArray(140, 140, 0);

while (($row = fgetcsv($handle)) !== false) {
    $lines[] = $row[0];
}

foreach ($lines as $index => $line) {
    $total += processLine($line, $index);
}

echo "<h2>Answer: $total</h2>";

fclose($handle);

function processLine($line, $lineIndex): int
{
    $value = 0;

    for($i = 0; $i < strlen($line); $i++) {
        $subLine = substr($line, $i, 3);

        if (!is_numeric(substr($subLine, 0, 1))) {
            continue;
        }

        $number = numberFromSubstr($subLine);

        $numberLength = strlen($number);

        // Add all gear values from the current line
        $value += gearValue($number, $lineIndex, $i);

        $i += $numberLength - 1;
    }

    return $value;
}

function numberFromSubstr($line): int
{
    $characters = str_split($line);

    $numberCharacters = '';

    foreach ($characters as $character) {
        if (!is_numeric($character)) {
            break;
        }

        $numberCharacters .= $character;
    }

    return intval($numberCharacters);
}

function gearValue($number, $lineIndex, $columnIndex): int
{
    global $lines;

    $currentValue = 0;

    $numberLength = strlen($number);

    $currentLine = $lines[$lineIndex];

    $currentLineCharacters = str_split($currentLine);

    // Check to the left of the number
    if (($currentLineCharacters[$columnIndex - 1] ?? '') === '*') {
        $markValue = getMark($lineIndex, $columnIndex - 1);
        if ($markValue > 0) {
            $currentValue += $markValue * $number;
        }

        if ($markValue === 0) {
            markCell($lineIndex, $columnIndex - 1, $number);
        }
    }

    // Check to the right of the number
    if (($currentLineCharacters[$columnIndex + $numberLength] ?? '') === '*') {
        $markValue = getMark($lineIndex, $columnIndex + $numberLength);
        if ($markValue > 0) {
            $currentValue += $markValue * $number;
        }

        if ($markValue === 0) {
            markCell($lineIndex, $columnIndex + $numberLength, $number);
        }
    }

    // Check above the number
    $previousLine = $lines[$lineIndex - 1] ?? '';

    $previousLineSubstr = substr($previousLine,  max($columnIndex - 1, 0), $numberLength + 2);

    $previousLineCharacters = str_split($previousLineSubstr);

    foreach ($previousLineCharacters as $previousLineCharacterIndex => $previousLineCharacter) {
        if ($previousLineCharacter === '*') {
            $markValue = getMark(
                $lineIndex - 1,
                $previousLineCharacterIndex + max($columnIndex - 1, 0),
            );

            if ($markValue > 0) {
                $currentValue += $markValue * $number;
            }

            if ($markValue === 0) {
                markCell(
                    $lineIndex - 1,
                    $previousLineCharacterIndex + max($columnIndex - 1, 0),
                    $number
                );
            }
        }
    }

    // Check below the number
    $nextLine = $lines[$lineIndex + 1] ?? '';

    $nextLineSubstr = substr($nextLine, max($columnIndex - 1, 0), $numberLength + 2);

    $nextLineCharacters = str_split($nextLineSubstr);

    foreach ($nextLineCharacters as $nextLineCharacterIndex => $nextLineCharacter) {
        if ($nextLineCharacter === '*') {
            $markValue = getMark(
                $lineIndex + 1,
                $nextLineCharacterIndex + max($columnIndex - 1, 0),
            );

            if ($markValue > 0) {
                $currentValue += $markValue * $number;
            }

            if ($markValue === 0) {
                markCell(
                    $lineIndex + 1,
                    $nextLineCharacterIndex + max($columnIndex - 1, 0),
                    $number
                );
            }
        }
    }

    return $currentValue;
}

// Get the marked value at the position of the gear
function getMark($lineIndex, $columnIndex): int
{
    global $marks;

    return $marks[$lineIndex][$columnIndex];
}

// Insert the value of the number into the position of the gear
function markCell($lineIndex, $columnIndex, $value): void
{
    global $marks;

    $marks[$lineIndex][$columnIndex] = $value;
}

function createRectangularArray($columns, $rows, $value): array
{
    $array = [];

    for ($i = 0; $i < $rows; $i++) {
        $array[$i] = [];

        for ($j = 0; $j < $columns; $j++) {
            $array[$i][$j] = $value;
        }
    }

    return $array;
}

require_once('../helpers.php'); printFile(__FILE__);
