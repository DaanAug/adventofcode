<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

$winningsPerCard =  array_fill(0, 208, 0);

$amountOfCards = array_fill(0, 208, 1);

$i = 0;

// Calculate winnings for each card.
while (($row = fgetcsv($handle, separator: ':')) !== false) {
    [$winningNumbersLine, $scratchedNumbersLine] = explode('|', trim($row[1]));

    $winningNumbers = explode(' ', trim($winningNumbersLine));
    $winningNumbers = array_filter($winningNumbers, fn($number) => intval($number) > 0);

    $scratchedNumbers = explode(' ', trim($scratchedNumbersLine));
    $scratchedNumbers = array_filter($scratchedNumbers, fn($number) => intval($number) > 0);

    $winningsPerCard[$i] = calculateValue($winningNumbers, $scratchedNumbers);

    $i++;
}

foreach($winningsPerCard as $index => $amount) {
    // Update the amount of cards.
    for ($i = 1; $i <= $amount; $i++) {
        $amountOfCards[$i + $index] += $amountOfCards[$index];
    }
}

$total = array_sum($amountOfCards);

echo "<h2>Answer: $total</h2>";

fclose($handle);

function calculateValue($winningNumbers, $scratchedNumbers): int
{
    // Each winning number is worth 1 point.
    $value = 0;

    foreach ($scratchedNumbers as $scratchedNumber) {
        if (in_array($scratchedNumber, $winningNumbers)) {
            $value ++;
        }
    }

    return $value;
}

require_once('../helpers.php'); printFile(__FILE__);
