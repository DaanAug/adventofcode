<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

while (($row = fgetcsv($handle, separator: ':')) !== false) {
    [$winningNumbersLine, $scratchedNumbersLine] = explode('|', trim($row[1]));

    $winningNumbers = explode(' ', trim($winningNumbersLine));
    $winningNumbers = array_filter($winningNumbers, fn($number) => intval($number) > 0);

    $scratchedNumbers = explode(' ', trim($scratchedNumbersLine));
    $scratchedNumbers = array_filter($scratchedNumbers, fn($number) => intval($number) > 0);

    $total += calculateValue($winningNumbers, $scratchedNumbers);
}

echo "<h2>Answer: $total</h2>";

fclose($handle);

function calculateValue($winningNumbers, $scratchedNumbers): int
{
    $value = 0;

    foreach ($scratchedNumbers as $scratchedNumber) {
        if (in_array($scratchedNumber, $winningNumbers)) {
            $value *= 2;

            if ($value === 0) {
                $value = 1;
            }
        }
    }

    return $value;
}

require_once('../helpers.php'); printFile(__FILE__);
