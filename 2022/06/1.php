<?php

$handle = fopen('./input.txt', 'r');

$text = trim(fgets($handle));

$answer = 0;

for ($i = 0; $i < strlen($text) - 4; $i++) {
    $sub = substr($text, $i, 4);

    if (isUnique($sub)) {
        $answer = $i + 4;
        break;
    }
}

echo "<h2>Answer: $answer</h2>";

function isUnique($sub) {
    $chars = str_split($sub);

    $unique = array_unique($chars);

    return count($unique) === 4;
}

require_once('../helpers.php'); printFile(__FILE__);
