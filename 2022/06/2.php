<?php

const MARKER_LENGTH = 14;

$handle = fopen('./input.txt', 'r');

$text = trim(fgets($handle));

$answer = 0;

for ($i = 0; $i < strlen($text) - MARKER_LENGTH; $i++) {
    $sub = substr($text, $i, MARKER_LENGTH);

    if (isUnique($sub)) {
        $answer = $i + MARKER_LENGTH;
        break;
    }
}

echo "<h2>Answer: $answer</h2>";

function isUnique($sub) {
    $chars = str_split($sub);

    $unique = array_unique($chars);

    return count($unique) === MARKER_LENGTH;
}

require_once('../helpers.php'); printFile(__FILE__);
