<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

while (($row = fgetcsv($handle)) !== false) {
    $firstLine = trim($row[0]);
    $secondLine = trim($row[1]);

    if (fullyContained($firstLine, $secondLine) || fullyContained($secondLine, $firstLine)) {
        $total++;
    }
}

echo "<h2>Answer: $total</h2>";

function fullyContained($firstLine, $secondLine) {
    $firstItems = explode('-', $firstLine);
    $secondItems = explode('-', $secondLine);

    $firstInRange = $firstItems[0] >= $secondItems[0] && $firstItems[0] <= $secondItems[1];
    $secondInRange = $firstItems[1] >= $secondItems[0] && $firstItems[1] <= $secondItems[1];

    if ($firstInRange && $secondInRange) {
        return true;
    }

    return false;
}

require_once('../helpers.php'); printFile(__FILE__);
