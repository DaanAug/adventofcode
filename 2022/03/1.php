<?php

$handle = fopen('./input.csv', 'r');

$total = 0;

while (($row = fgetcsv($handle)) !== false) {
    $line = trim($row[0]);

    $total+= calculateValue($line);
}

echo "<h2>Answer: $total</h2>";

fclose($handle);

function calculatePriority($item) {
    // Lowercase item types have priorities 1 through 26
    if (ctype_lower($item)) {
        return ord($item) - ord('a') + 1;
    }
    // Uppercase item types have priorities 27 through 52
    elseif (ctype_upper($item)) {
        return ord($item) - ord('A') + 27;
    }
    // Invalid item type
    else {
        return 0;
    }
}

function calculateValue($rucksack) {    
    // Split the rucksack string into two compartments
    $length = strlen($rucksack) / 2;
    $compartments1 = str_split(substr($rucksack, 0, $length));
    $compartments2 = str_split(substr($rucksack, $length));

    // Find the intersection of characters in both compartments
    $commonItems = array_intersect($compartments1, $compartments2);

    foreach ($commonItems as $item) {
        return calculatePriority($item);
    }
}

require_once('../helpers.php'); printFile(__FILE__);
