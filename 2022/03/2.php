<?php

$handle = fopen('./input.csv', 'r');

function getGroup($handle, int $groupSize) {
    $group = [];
    
    $loop = 0;

    while ($loop < $groupSize && ($row = fgetcsv($handle)) !== false) {
        $group[] = trim($row[0]);
        
        $loop++;
    }

    return $group;
}

$total = 0;

for ($i = 0; $i < 100; $i++) {
    $group = getGroup($handle, 3);

    $firstItem = str_split($group[0]);

    foreach ($firstItem as $item) {
        $containsInSecond = str_contains($group[1], $item);

        $containsInThird = str_contains($group[2], $item);

        if ($containsInSecond && $containsInThird) {
            $total += calculatePriority($item);

            break;
        }
    }
}

echo "<h2>Answer: $total</h2>";

function calculatePriority($item) {
    // Lowercase item types have priorities 1 through 26
    if (ctype_lower($item)) {
        return ord($item) - ord('a') + 1;
    }
    // Uppercase item types have priorities 27 through 52
    elseif (ctype_upper($item)) {
        return ord($item) - ord('A') + 27;
    }
    // Invalid item type
    else {
        return 0;
    }
}

require_once('../helpers.php'); printFile(__FILE__);
