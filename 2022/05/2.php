<?php

$handle = fopen('./input.csv', 'r');

$input = [
    ['N', 'B', 'D', 'T', 'V', 'G', 'Z', 'J'],
    ['S', 'R', 'M', 'D', 'W', 'P', 'F'],
    ['V', 'C', 'R', 'S', 'Z'],
    ['R', 'T', 'J', 'Z', 'P', 'H', 'G'],
    ['T', 'C', 'J', 'N', 'D', 'Z', 'Q', 'F'],
    ['N', 'V', 'P', 'W', 'G', 'S', 'F', 'M'],
    ['G', 'C', 'V', 'B', 'P', 'Q'],
    ['Z', 'B', 'P', 'N'],
    ['W', 'P', 'J'],
];

while(($row = fgetcsv($handle)) !== false) {
    $row = trim($row[0]);

    $row = explode(' ', $row);
    $amout = $row[1];
    $from = $row[3];
    $to = $row[5];

    $fromInput = $input[$from -1];
    $toInput = $input[$to -1];

    [$newFrom, $newTo] = operate($fromInput, $toInput, $amout);

    $input[$from -1] = $newFrom;
    $input[$to -1] = $newTo;
}

echo "<h2>Answer: ";

foreach ($input as $value) {
    echo end($value);
}

echo "</h2>";

function operate($from, $to, $times) {
    $items = array_splice($from, -1 * $times);

    $to = array_merge($to, $items);
    
    return [$from, $to];
}


require_once('../helpers.php'); printFile(__FILE__);
