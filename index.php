<!DOCTYPE html>
<html>
<head>
    <title>Advent of code</title>
</head>
<body>
    <h1>Advent of code</h1>
    <ul>
    <?php
        $path = './'; // specify the path to scan
        $dirs = array_filter(glob($path . '*'), 'is_dir'); // get all directories in the path

        foreach ($dirs as $dir) {
            $dirName = basename($dir); // get the name of the directory
            echo "<li><a href='$dir'>$dirName</a></li>"; // display the directory name as a link
        }
        ?>
    </ul>
</body>
</html>
